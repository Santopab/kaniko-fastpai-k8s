from fastapi import FastAPI

app = FastAPI(
    title="OCR Service",
    # if not custom domain
    # openapi_prefix="/prod"
)


@app.get("/ocr-api")
def read_root():
    return {"status": "OCR - Service is running"}

@app.get("/ping", summary="Check that the service is operational")
def pong():
    """
    Sanity check - this will let the user know that the service is operational.
    It is also used as part of the HEALTHCHECK. Docker uses curl to check that the API service is still running, by exercising this endpoint.
    """
    return {"ping": "pong!"}